import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
  id: number;
  name: string;
  tel: string;
  gender: string;
}
export const useAddressBookStore = defineStore('address_book', () => {

const address = ref<AddressBook>({
  id: 0,
  name: "",
  tel: "",
  gender: "Male",
});
let lastId = 1;
const addressList = ref<AddressBook[]>([]);
const isAddNew = ref(false);
function save() {
  // address.value.id = lastId++;
  if (address.value.id > 0) {
    //edit
    const editedIndex = addressList.value.findIndex(
      (item) => item.id === address.value.id
    );
    addressList.value[editedIndex] = address.value;
  } else {
    //add new
    addressList.value.push({ ...address.value, id: lastId++ });
  }
  isAddNew.value =false
  address.value = {
    id: 0,
    name: "",
    tel: "",
    gender: "Male",
  };
}
function edit(id: number) {
  isAddNew.value = true
  const editedIndex = addressList.value.findIndex((item) => item.id === id);
  //copy object JSON.parse(JSON.stringify(object))
  address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]));
}

function remove(id: number) {
  const removeIndex = addressList.value.findIndex((item) => item.id === id);
  //copy object JSON.parse(JSON.stringify(object))
  addressList.value.splice(removeIndex, 1);
}

function cancle(){
  isAddNew.value =false
  address.value = {
    id: 0,
    name: "",
    tel: "",
    gender: "Male",
  };
}

  return { address,addressList,save,isAddNew,cancle,edit,remove}
})